import React, { Component } from 'react';
import ListPersonComponent from './ListPersonComponent';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import PersonComponent from './PersonComponent';

class PersonApp extends Component {
    render() {
        return (
            <Router>
                <>
                    <br/>
                    <h1>Api Spring Boot Application</h1>
                    <br/>
                    <Switch>
                        <Route path="/" exact component={ListPersonComponent} />
                        <Route path="/person" exact component={ListPersonComponent} />
                        <Route path="/person/:id" component={PersonComponent} />
                    </Switch>
                </>
            </Router>
        )
    }
}
export default PersonApp

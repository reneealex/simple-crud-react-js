import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import PersonService from '../service/PersonService';

class PersonComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            name: '',
            address: ''
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
        this.addCancelClicked = this.addCancelClicked.bind(this)
    }
    componentDidMount() { 
        if (this.state.id === -1) {
            return
        }
        PersonService.getPersonById(this.state.id)
            .then(response => this.setState({
                name: response.data.name,
                address: response.data.address
            }))
    }
    addCancelClicked() {
      this.props.history.push(`/person/`)
    }
    validate(values) {
        let errors = {}
        if (!values.name) {
            errors.name = 'Enter a Name'
        } else if (values.name.length < 5) {
            errors.name = 'Enter at least 5 Characters in Name'
        }
        return errors
    }
    onSubmit(values) { 
        let person = {
            id: this.state.id,
            name: values.name,
            address: values.address
        }
        if (this.state.id == -1) {
            PersonService.createPerson(person)
                .then(() => this.props.history.push('/'))
        } else {
            PersonService.updatePerson(this.state.id, person)
                .then(() => this.props.history.push('/person'))
        } 
    }
    render() {
        let { name, address, id } = this.state 
        return (
            <div>
                <h3>Person</h3>
                <div className="container">
                    <Formik
                        initialValues={{ id: id, name: name, address: address }} 
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="description" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Name</label>
                                        <Field className="form-control" type="text" name="name" 
/>
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label>Address</label>
                                        <Field className="form-control" type="text" name="address" 
/>
                                    </fieldset>
                                    <div className="form-inline">
                                      <div className="form-group mb-2">
                                        <button className="btn btn-success" type="submit">Save</button>
                                      </div>
                                      <div className="form-group mx-sm-3 mb-2">
                                        <button className="btn btn-warning" onClick={this.addCancelClicked}>Cancel</button>
                                      </div>
                                    </div>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        )
    }
}
export default PersonComponent 
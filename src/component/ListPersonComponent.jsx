import React, { Component } from 'react'
import PersonService from '../service/PersonService';

class ListPersonComponent extends Component {
    constructor(props) {
      super(props)
      this.state = {
          people: [],
          message: null
      }
      this.deletePersonClicked = this.deletePersonClicked.bind(this)
      this.updatePersonClicked = this.updatePersonClicked.bind(this)
      this.addPersonClicked = this.addPersonClicked.bind(this)
      this.refreshPersons = this.refreshPersons.bind(this)
    }

    componentDidMount() { 
        this.refreshPersons();
    }

    refreshPersons() {
      PersonService.getAllPerson()
            .then(
                response => { 
                    this.setState({ people: response.data })
                }
            )
    }
    
    deletePersonClicked(id) {
        PersonService.deletePerson(id)
            .then(
                response => {
                    this.setState({ message: `Delete person ${id} Successful` })
                    this.refreshPersons()
                }
            )
    }

    addPersonClicked() {
        this.props.history.push(`/person/-1`)
    }

    updatePersonClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/person/${id}`)
    }

   render() { 
      return ( 
          <div className="container">
              {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
              <div className="container">     
                  <table className="table">
                      <thead>
                          <tr>
                              <th>Id</th>
                              <th>Name</th>
                              <th>Address</th>
                              <th></th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                            {
                                this.state.people.map(
                                  person =>
                                        <tr key={person.id}>
                                            <td>{person.id}</td>
                                            <td>{person.name}</td>
                                            <td>{person.address}</td>
                                            <td><button className="btn btn-success" onClick={() => this.updatePersonClicked(person.id)}>Update</button></td>
                                            <td><button className="btn btn-warning" onClick={() => this.deletePersonClicked(person.id)}>Delete</button></td>
                                        </tr>
                                )
                            }
                        </tbody>
                  </table>
                  <div className="row">
                    <button className="btn btn-success" onClick={this.addPersonClicked}>Add</button>
                  </div>
              </div>              
          </div>         
      )
  }
}
export default ListPersonComponent
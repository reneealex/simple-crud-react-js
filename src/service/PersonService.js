import axios from 'axios'
//const INSTRUCTOR = 'in28minutes'
const COURSE_API_URL = 'http://localhost:8890/' 
//const INSTRUCTOR_API_URL = `${COURSE_API_URL}/instructors/${INSTRUCTOR}`

class PersonService {
    getAllPerson() {
        return axios.get(`${COURSE_API_URL}`);
    }
    
    getPersonById(id) { 
      return axios.get(`${COURSE_API_URL}/person/${id}`);
    }

    deletePerson(id) { 
      return axios.delete(`${COURSE_API_URL}/person/${id}`);
    } 

    updatePerson(id, person) {
      return axios.put(`${COURSE_API_URL}/person/${id}`, person);
    }

    createPerson(person) { 
      return axios.post(`${COURSE_API_URL}/person/`, person);
    }
}
export default new PersonService()